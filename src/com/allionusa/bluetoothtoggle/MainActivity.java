package com.allionusa.bluetoothtoggle;

import android.support.v7.app.AppCompatActivity;
import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Bundle;
import android.os.ParcelUuid;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;

import com.allionusa.bluetoothtoggle.BluetoothConnector;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;



@SuppressLint("NewApi")
public class MainActivity extends AppCompatActivity {
	private static final int REQUEST_ENABLE_BT = 1;
	private static final String TAG = "MyActivity";
	private static final UUID DEFAULT_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
	private Button connOne;
	private Button connTwo;
	private Button connThree;
	static final String KeyboardMAC = "00:1F:20:4D:CD:5D";
	static final String MouseMAC = "";
	static final String Jambox = "00:02:3C:40:82:42";
	static UUID MY_UUID;
	private BluetoothAdapter BTAdapter;
	private Set<BluetoothDevice> pairedDevice;
	private ArrayAdapter<String> BTArrayAdapter;
	private TextView text;
	private ListView myListView;
	private int totalDevices;
	private List<UUID> l;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
			
		BTAdapter = BluetoothAdapter.getDefaultAdapter();
		pairedDevice = BTAdapter.getBondedDevices();
		//text = (TextView) findViewById(R.id.textView1);
		totalDevices = 0;
		
		if (BTAdapter == null) {
			Toast.makeText(getApplicationContext(), "You NO HAVE BT", Toast.LENGTH_LONG).show();
		} else {
			connOne = (Button)findViewById(R.id.button1);
			connOne.setOnClickListener(new View.OnClickListener()
			{	@Override
				public void onClick(View v)
				{	//enable BT
					on(v);
					// Connect 1 device
					list(v);
					BluetoothDevice kb = BTAdapter.getRemoteDevice(KeyboardMAC);
					Log.d(TAG, "Bluetooth device at 0: "+kb.getName()+" "+kb.getAddress());
					List<UUID> UUIDlist = getListOfUUID();
					BluetoothConnector btConn = new BluetoothConnector(kb, false, BTAdapter, UUIDlist);
					try {
						btConn.connect();
					} catch (IOException e) {
						Log.d(TAG,"IO Exception after connect: "+e.getMessage());
						e.printStackTrace();
						
					}
					//connect(v, 1);
				}
			});
			connTwo = (Button)findViewById(R.id.button2);
			connTwo.setOnClickListener(new View.OnClickListener()
			{	@Override
				public void onClick(View v)
				{	// enable BT
					 on(v);
					 // Connect 2 device
				}
			});
			connThree = (Button)findViewById(R.id.button3);
			connThree.setOnClickListener(new View.OnClickListener()
			{	@Override
				public void onClick(View v)
				{	// enable BT
					 on(v);
					 // Connect 3 device
				}
			});
		}
		
		myListView = (ListView)findViewById(R.id.listView1);
		BTArrayAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1);
		myListView.setAdapter(BTArrayAdapter);
		
		
		
	}
	
	public String getDevice(Set<BluetoothDevice> bt, int spot) {
		String device = null;
		int inc = 0;
		for (BluetoothDevice dev : bt) {
			device = dev.getAddress();
			inc++;
			if (inc == spot)
				break;
		}
		return device;
	}
	
	public void on(View view) {
		if (!BTAdapter.isEnabled()) {
			BTAdapter.enable();
			BTAdapter.cancelDiscovery();
			Toast.makeText(getApplicationContext(), "Enabling BT", Toast.LENGTH_SHORT).show();
		}
		else {
			BTAdapter.cancelDiscovery();
			//Toast.makeText(getApplicationContext(), "BT Already Enabled", Toast.LENGTH_SHORT).show();
		}
	}
	
	public void list(View view) {
		pairedDevice = BTAdapter.getBondedDevices();
		BTArrayAdapter.clear();
		for (BluetoothDevice device : pairedDevice){
			BTArrayAdapter.add(device.getName()+ "\n" + device.getAddress());
		}
		totalDevices = BTArrayAdapter.getCount();
		Toast.makeText(getApplicationContext(),"Found "+Integer.toString(totalDevices)+" Paired Devices",
				              Toast.LENGTH_SHORT).show();
	}
	
	public List<UUID> getListOfUUID() {
		l = new ArrayList<UUID>();
		int count = 0;
		//BluetoothDevice btDevice = BTAdapter.getRemoteDevice(KeyboardMAC);
		Method getUuidsMethod;
		try {
			getUuidsMethod = BluetoothAdapter.class.getDeclaredMethod("getUuids", null);
			ParcelUuid[] uuids = (ParcelUuid[]) getUuidsMethod.invoke(BTAdapter, null);
			for (ParcelUuid uuid: uuids) {
	            Log.d(TAG, "UUID: " + uuid.getUuid().toString());
	            l.add(count, uuid.getUuid());
	            count++;
	        }
			l.add(count, UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"));
			count++;
		}  catch (NoSuchMethodException e2) {
			Log.d(TAG, e2.getMessage());
			e2.printStackTrace();
		} catch (IllegalAccessException e) {
			Log.d(TAG, e.getMessage());
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			Log.d(TAG, e.getMessage());
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			Log.d(TAG, e.getMessage());
			e.printStackTrace();
		}
		Log.d(TAG, "count: "+Integer.toString(count));

		return l;
	}
	
	public void connect(View view, int numDevices) {
		Toast.makeText(getApplicationContext(),"Connecting "+Integer.toString(numDevices)+" Paired Devices",
	              Toast.LENGTH_SHORT).show();
		// figure out way to connect based on MAC
		BluetoothDevice device = BTAdapter.getRemoteDevice(KeyboardMAC);
		BluetoothSocket tmp = null;
        BluetoothSocket mmSocket = null;
        Method getUuidsMethod;
		try {
			getUuidsMethod = BluetoothAdapter.class.getDeclaredMethod("getUuids", null);
			ParcelUuid[] uuids = (ParcelUuid[]) getUuidsMethod.invoke(BTAdapter, null);
			for (ParcelUuid uuid: uuids) {
	            Log.d(TAG, "UUID: " + uuid.getUuid().toString());
	        }
		} catch (NoSuchMethodException e2) {
			e2.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}

        

        
        
        /*try {
            // Use the UUID of the device that discovered 
            Method m = device.getClass().getMethod("createRfcommSocket",
                    new Class[] { int.class });
            mmSocket = (BluetoothSocket) m.invoke(device, Integer.valueOf(1));
            Log.d(TAG, "Creating RFComm Socket with new Method");
            Thread.sleep(500);
            mmSocket.connect();
        }
        catch (NullPointerException e)
        {
            Log.d(TAG, " UUID from device is null, Using Default UUID, Device name: " + device.getName());
            try {
                tmp = device.createRfcommSocketToServiceRecord(DEFAULT_UUID);
                tmp.connect();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
        catch (NoSuchMethodException e) {
        	Log.d(TAG, "No such method");
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			Log.d(TAG,"Illegal Access");
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			Log.d(TAG, "Illegal Argument");
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			Log.d(TAG, "Invocation Target");
			e.printStackTrace();
		} catch (IOException e) {
			Log.d(TAG, "IO Exception: "+e.getMessage());
			e.printStackTrace();
			try {
                tmp = device.createRfcommSocketToServiceRecord(DEFAULT_UUID);
                Log.d(TAG, "Creating RF Comm socket with default UUID");
                Thread.sleep(500);
                tmp.connect();
                //tmp = device.createInsecureRfcommSocketToServiceRecord(UUID.fromString("0000111f-0000-1000-8000-00805f9b34fb"));
                //Log.d(TAG, "Creating RF Comm socket with default UUID");
                //tmp.connect();
                //tmp = device.createInsecureRfcommSocketToServiceRecord(UUID.fromString("00001112-0000-1000-8000-00805f9b34fb"));
                //Log.d(TAG, "Creating RF Comm socket with other uuid");
                //tmp.connect();
                ///tmp = device.createInsecureRfcommSocketToServiceRecord(UUID.fromString("0000110a-0000-1000-8000-00805f9b34fb"));
                //Log.d(TAG, "Creating RF Comm socket with other uuid");
                //tmp.connect();
            } catch (IOException e1) {
            	Log.d(TAG, "IO Exception");
                e1.printStackTrace();
            } catch (InterruptedException e1) {
				e1.printStackTrace();
			}
		} catch (InterruptedException e) {
			Log.d(TAG, "Interrupt Exception");
			e.printStackTrace();
		} */
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}